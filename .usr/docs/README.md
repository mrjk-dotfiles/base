
# Scenarios (WIP)

Bootstrap the whole thing:
```
curl -sL https://raw.githubusercontent.com/TheLocehiliosan/yadm/master/yadm | sh -s clone https://framagit.org/mrjk/homelan/home.git -w ~/.local/share/shell.git --bootstrap
```

# Testing/CI

```
cd ~
#docker run --rm -i -t -v $PWD:/root  debian /bin/bash
docker run --rm -i -t -v $PWD:/pwd  --entrypoint /bin/bash debian 
```

```
apt-get update; apt-get install curl git tree vim -y; cd /root; 
mkdir ~/.local/share/shell.git
curl -sL https://raw.githubusercontent.com/mrjk/yadm/master/yadm | YADM_OVERRIDE_REPO=.local/share/yadm.git YADM_DIR=.usr/shell/yadm bash -s  clone https://framagit.org/mrjk-dotfiles/base.git --bootstrap
```

 The one shot repeatition:
```
rm -rf opt .gnupg .ssh .local .usr .yadm/ .gitmodules .gitconfig  .*bak* ; find /root -type l | xargs rm ;  ls -ahl ; echo 'Cleaned'; curl -sL https://raw.githubusercontent.com/mrjk/yadm/master/yadm | YADM_OVERRIDE_REPO=.local/share/yadm.git YADM_DIR=.usr/shell/yadm bash -s  clone https://framagit.org/mrjk-dotfiles/base.git --bootstrap ; bash
```




# Notes:

On debian 9, in a minimal docker instance, env is:
```
HOME=/root
HOSTNAME=43fe49e71c7e
OLDPWD=/root/.usr/shell/yadm
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PWD=/root
SHLVL=2
TERM=xterm
_=/usr/bin/env
```
There are no curl or git default too. A scp feature should be nice ...

# Bugs:

dans ellipsis:
```
111     msg.print "linking $(path.relative_to_packages "$src") -> $(path.relative_to_home "$dest")"     
shows: linking /root/homeland-base.git/config/lib -> ~/.config/lib
shows: linking /root/$MISSING_HERE/homeland-base.git/config/lib -> ~/.config/lib
```
 17 fs.link_files() {
```
# Does not accept another argument :/ But fs.link_file do (which is buggy too)
```

 27 fs.link_file() {
```
# This function does not treat correctly when target is a dir
# Links should be RELATIVES, just add: ln -rs ... instead of ln -s ...
```

This variable does not parse ~ ... something hardcoded with it ...
ELLIPSIS_PACKAGES=.usr/shell/mod


# Tests

```
curl -sL https://raw.githubusercontent.com/TheLocehiliosan/yadm/master/yadm | sh clone https://framagit.org/mrjk/homelan/home.git 

yadm

git clone https://framagit.org/mrjk/homelan/home.git ~/.yadm-project
ln -s ~/.yadm-project/yadm ~/bin/yadm
curl -sL ellipsis.sh | PACKAGES='vim zsh' sh
curl -sL https://raw.githubusercontent.com/TheLocehiliosan/yadm/master/yadm | sh
```

# How to install
There are differents methods to install the code, but all of them are dangerous. Read carefuly the documentation ;)

## Install yadm
You need to install yadm:
```
# As normal user
curl -fL https://github.com/TheLocehiliosan/yadm/raw/master/yadm > /tmp/yadm && chmod +x /tmp/yadm; 

# As root
sudo curl -fLo /usr/local/bin/yadm https://github.com/TheLocehiliosan/yadm/raw/master/yadm && chmod a+x /usr/local/bin/yadm
```
See here for more informations: https://thelocehiliosan.github.io/yadm/docs/install

## Read only mode, with clean ```$HOME```
Use this installation method when you can trash all you $home:
```
cd $HOME

# Be careful, do this with you regular user
rm -rf $HOME/{.,*} 2> /dev/null

# If you installed in /tmp yadm
/tmp/yadm clone https://gitlab.savoirfairelinux.com/rcordier/dotfiles.git . && exit

# or if you installed yadm as root
yadm clone https://gitlab.savoirfairelinux.com/rcordier/dotfiles.git . && exit
```
At this stage you should have been logged off. Login again to see the changes :-) If you installed yadm in /tmp, you can now delete it as yadm is bundled into the ```$HOME``` files.
```
rm /tmp/yadm
```


## Read only mode, when files already exists in ```$HOME```
If you have stuffs in your ```$HOME``` and you don't want to trash them:
```
/tmp/yadm init ; /tmp/yadm remote add origin https://gitlab.savoirfairelinux.com/rcordier/dotfiles.git; /tmp/yadm fetch --all;  /tmp/yadm reset --hard origin/master; exit
```

## Edition mode.
You need to follow the previous steps, then, if you want to work on master:
```
yadm remote set-url origin git@gitlab.savoirfairelinux.com:rcordier/dotfiles.git; yadm fetch --all
```

If you want to work on the ```dev``` branch:
```
yadm checkout -b dev
```



